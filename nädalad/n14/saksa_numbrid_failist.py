# -*- coding: ISO-8859-4 -*-
# Dictionary (s�nastiku) n�ide.
# S�nastik moodustatakse eesti ja saksakeelstest arvudest,
# p�rast saab saksa keelt �ppida :)
# Idee: on s�nastik, kus eestikeelne s�na on v�tmeks ja saksakeelne vaste v��rtuseks.
# Et eesti s�nu juhuslikult valida, teisendatakse v�tmed listiks ja sealt valitakse
# suvaline element random.choice-funktsiooni abil.
# Edasi j�rgneb elementaarne kontroll ja kommentaar.
# Loomulikult v�ivad failis olla ka muud s�nad kui arvud

import random

print("Tere! Hakkan Sulle �petama saksa keelt.")
print("Kui oled �pitavad s�nad tekstifaili kirjutanud, v�id pihta hakata.")
print("Arvuti kirjutab s�na eesti keeles, Sina pead vastama saksa keeles.")
# K�igepealt tehakse valmis �ks s�nastik. Andmed loetakse tekstifailist.
# T�hja s�nastiku algv��rtustamine
est2ger = {}
fnimi = input("Kus failis on s�nad?")
fm = open(fnimi)
# Failis loetakse s�napaarid, jagatakse t�hiku kohalt ja kirjutatakse s�nastikku.
for rida in fm:
    eesti,saksa = rida.split()
    est2ger[eesti] = saksa

# Algab harjutamine
# V�tmetest (eesti s�nadest) tehakse list.
temp = est2ger.keys()    # Selle k�suga tekib nn view, listiks kirjutatakse see j�rgmise for-ts�kliga
arvud = []
for arv in temp:
     arvud.append(arv)
soov = "j"
while soov == "j":
# Suvalise s�na v�tmine listist
    arv = random.choice(arvud)
    vastus = input(arv + ' ')
# Listist v�etud s�na on s�nastikule n� indeksiks (v�tmeks).
    if est2ger[arv] == vastus.lower():
        print("�ige!")
    else:
        print("Vale! �ige vastus oleks", est2ger[arv])
    soov = input("Kas j�tkame? [j/e]")

