import texttable as tt

def ex1():
    """Programmi töö aluseks on andmed odaviske võistluse tulemuste kohta. Selleks on sportlase nimi ja kolme viske tulemused.
    Andmed on nö lihtsustatud, ehk parim vise tuleb leida kolme viske hulgast.
    Leida võitja."""
    file = open("oda_uus.txt", "r")

    lines = list()
    results = {}
    best_result = list()

    for line in file:
        # print(line.strip("\n"))
        lines.append(line.strip("\n"))
        line = line.strip("\n")
        data = line.split(" ")
        first_name = data[0]
        last_name = data[1]
        # print(first_name)
        results[first_name + " " + last_name] = list()

        for index in range(2, data.__len__()):
            results[first_name + " " + last_name].append(data[index])

        results[first_name + " " + last_name].sort(reverse=True)

    # print(results)
    for result in results:
        if len(best_result) == 0:
            best_result.append(result)
            best_result.append(results[result][0])
        else:
            if float(results[result][0]) > float(best_result[1]):
                best_result.clear()
                best_result.append(result)
                best_result.append(results[result][0])
    # print(data.__len__()-1)

    print(results)
    # print(data)
    print(best_result)


def percent_to_grade(percent):
    if percent in range(91, 101):
        return 5
    elif percent in range(81, 91):
        return 4
    elif percent in range(71, 81):
        return 3
    elif percent in range(61, 71):
        return 2
    elif percent in range(51, 61):
        return 1
    else:
        return 0


def ex2():
    """Uudne hindamissüsteem punktide, cm, ja sekundite teisendamisel hinneteks on järgmine:
    Hinde panemise aluseks ei ole hindaja "absoluutne tõde", vaid hinnatavate õpilaste hulgast
    parim tulemus, mis on 100%. Ülejäänute hinne leitakse protsendi järgi järgmiselt:
    hinne "5" 91-100%, hinnne "4" 81-90% jne ning läbikukkunud alla 51%.
    Koosta õpetaja tööd lihtsustav programm, mis pakub tulemuste järgi hinde.

    Failis tulemused_mn.txt on kõrgushüppes saadud tulemused, laste nimed ja sugu (M/N).
    Trüki välja hinde kolm saanud laste nimed ja tulemused. Loe ka kokku, mitu 3 pandi.
    Hindamine toimub poiste ja tüdrukute arvestuses oma parima järgi."""

    file = open("tulemused_mn.txt", "r")

    lines = list()

    results = {}
    best_result = {"M": 0, "N": 0}

    for line in file:
        # print(line.strip("\n"))
        line = line.strip("\n")
        lines.append(line)
        data = line.split(" ")
        first_name = data[0]
        last_name = data[1]
        # print(first_name)
        results[first_name + " " + last_name] = {"result": data[2], "gender": data[3]}
        if best_result[data[3]] < int(data[2]):
            best_result[data[3]] = int(data[2])

    print("Naised:")
    for result in results:
        if results[result]["gender"] == "N":
            name = result
            percent = int(100 * round(int(results[result]["result"]) / int(best_result["N"]), 2))
            print(name + " - " + str(percent) + "% - " + str(percent_to_grade(percent)))
    print("")
    print("Mehed:")
    for result in results:
        if results[result]["gender"] == "M":
            name = result
            percent = int(100 * round(int(results[result]["result"]) / int(best_result["M"]), 2))
            print(name+" - "+str(percent) + "% - " + str(percent_to_grade(percent)) )
    # print(results)
    print("")
    print(best_result)


def ex3():
    """Kassiklubi Felix vajab abiprogrammi oma kiisukeste andmete töötlemiseks. Iga kassi kohta on tekstifailis
    nimi, värvus ja saba pikkus. Viimane on eriti suure tähtsusega ;) Andmed on faili felix.txt
    Kirjuta programm, mis aitab klubi omanikul leida teda huvitavat värvi kasside keskmine sabapikkus.
    Programm peab laskma kasutajal värvi sisestada ja reageerima ka siis adekvaatselt, kui sellist värvi loomi polegi.
    """
    file = open("felix.txt", "r")

    cats = {}
    findcolor = "hall"

    for line in file:
        line = line.strip("\n")
        data = line.split(" ")
        color = data[1]
        length = data[2]
        name = data[0]
        cats[name] = {"color": color, "length": length}
        #print(color)
        #print(line)

    for cat in cats:
        # print(cats[cat]["color"])
        if cats[cat]["color"] == findcolor:
            print(cat + " " +cats[cat]["length"])

def print_table(data):
    format = "{:10}"

    tab = tt.Texttable()
    headings = ['Linn', 'Mehed', 'Costs', 'Unit_Costs']
    tab.header(headings)
    names = ['bar', 'chocolate', 'chips']
    weights = [0.05, 0.1, 0.25]
    costs = [2.0, 5.0, 3.0]
    unit_costs = [40.0, 50.0, 12.0]
    print(list(data.values())[0]["edu_men"])
    print(list(data.values()))
    #print(dict(data.values()))
    # print(list(data.values())[0]["edu_men"])
    index = 0
    for row in zip(data, list(data.values())[0], unit_costs,costs):
        tab.add_row(row)

    s = tab.draw()
    print(s)

    #for row in data:
        #print(format.format(row), end="")
        #print(format.format(int(data[row]["edu_men"])), end="")
        #print("")

def ex4():
    """Tekstifailis haridus.txt on andmed rahvastiku hariduse kohta erinevates linnades ja valdades. Ühes reas on valla
    või linna nimi, üksuse liik, kõrghardusega meeste arv, meeste koguarv, kõrghardusega naiste arv ja naiste koguarv
    (positiivsed täisarvud). Võib eeldada, et fail vastab kirjeldusele.
    Leida omavalitsus, kus on suurim kõrgharidusega meeste protsent meeste koguarvust. Trükkida välja omavalitsuse nimi,
    liik ja protsent.
    Loendada, mitmes linnas on kõrghariduseta naisi rohkem kui mehi.
    Trükkida tabel nende omavalitsusüksuste andmetega (nimi, liik, kõrgharidusega inimeste arv), kus kõrgharidusega
    inimeste arv on suurem mingist etteantud suurusest. Viimase sisestab kasutaja. Kui selliseid omavalitsusi ei ole,
    anda sobilik teade."""

    file = open("haridus.txt", "r")

    results = {}

    for line in file:
        line = line.strip("\n")
        data = line.split(" ")
        edu_men = data[2]
        men = data[3]
        edu_women = data[4]
        women = data[5]
        results[data[0]+" "+data[1]] = {"edu_men": int(edu_men), "edu_women": int(edu_women)}
        # print(line)
    print_table(results)
    print(results)

#ex4()