14. nädal

Kõigis ülesannetes mainitud andmefailide asukohaks on Praktikumi ülesannete kaust.

Ülesanne 1 Odaviske võistlus
Programmi töö aluseks on andmed odaviske võistluse tulemuste kohta. Selleks on sportlase nimi ja kolme viske tulemused.
Andmed on nö lihtsustatud, ehk parim vise tuleb leida kolme viske hulgast.
Leida võitja.

Andmed on tekstifailis nimega oda_uus.txt.


Ülesanne 2 Hindamine
Uudne hindamissüsteem punktide, cm, ja sekundite teisendamisel hinneteks on järgmine:
Hinde panemise aluseks ei ole hindaja "absoluutne tõde", vaid hinnatavate õpilaste hulgast
parim tulemus, mis on 100%. Ülejäänute hinne leitakse protsendi järgi järgmiselt:
hinne "5" 91-100%, hinnne "4" 81-90% jne ning läbikukkunud alla 51%.
Koosta õpetaja tööd lihtsustav programm, mis pakub tulemuste järgi hinde.

Failis tulemused_mn.txt on kõrgushüppes saadud tulemused, laste nimed ja sugu (M/N).
Trüki välja hinde kolm saanud laste nimed ja tulemused. Loe ka kokku, mitu 3 pandi.
Hindamine toimub poiste ja tüdrukute arvestuses oma parima järgi.


Ülesanne 3 Kassiklubi
Kassiklubi Felix vajab abiprogrammi oma kiisukeste andmete töötlemiseks. Iga kassi kohta on tekstifailis
nimi, värvus ja saba pikkus. Viimane on eriti suure tähtsusega ;) Andmed on faili felix.txt
Kirjuta programm, mis aitab klubi omanikul leida teda huvitavat värvi kasside keskmine sabapikkus.
Programm peab laskma kasutajal värvi sisestada ja reageerima ka siis adekvaatselt, kui sellist värvi loomi polegi.


Üleanne 4 Haridus
Tekstifailis haridus.txt on andmed rahvastiku hariduse kohta erinevates linnades ja valdades. Ühes reas on valla
või linna nimi, üksuse liik, kõrghardusega meeste arv, meeste koguarv, kõrghardusega naiste arv ja naiste koguarv
(positiivsed täisarvud). Võib eeldada, et fail vastab kirjeldusele.
Leida omavalitsus, kus on suurim kõrgharidusega meeste protsent meeste koguarvust. Trükkida välja omavalitsuse nimi,
liik ja protsent.
Loendada, mitmes linnas on kõrghariduseta naisi rohkem kui mehi.
Trükkida tabel nende omavalitsusüksuste andmetega (nimi, liik, kõrgharidusega inimeste arv), kus kõrgharidusega
inimeste arv on suurem mingist etteantud suurusest. Viimase sisestab kasutaja. Kui selliseid omavalitsusi ei ole,
anda sobilik teade.