# -*- coding: ISO-8859-4 -*-
# S�nastiku (dictionary) kasutamise n�ide.
# S�nastik moodustatakse eesti ja saksakeelstest arvudest,
# kus eestikeelne s�na on v�tmeks ja saksakeelne vaste v��rtuseks.
# Eestikeelse s�na juhuslikuks valimiseks teisendatakse v�tmed listiks
# ja sealt valitakse suvaline element random.choice()-funktsiooni abil.
# J�rgneb elementaarne kontroll ja kommentaar.

import random
# K�igepealt tehakse valmis �ks konstantne s�nastik, mille v�iks ka tekstifailist sisse lugeda.
est2ger = {'�ks':'ein','kaks':'zwei','kolm':'drei','neli':'vier','viis':'f�nf','kuus':'sechs','seitse':'sieben','kaheksa':'acht','�heksa':'neun','k�mme':'zehn','�ksteist':'elf','kaksteist':'zw�lf'}

print("Tere! Hakkan Sulle �petama arve saksa keeles.")
print("Esialgu piirdume kaheteistk�mne esimesega.")
print("Arvuti kirjutab arvu eesti keeles, Sina pead vastama saksa keeles.")

# V�tmetest (eesti s�nadest) tehakse list.
temp = est2ger.keys()    # Selle k�suga tekib nn view, listiks kirjutatakse see j�rgmise for-ts�kliga
arvud = []
for arv in temp:
     arvud.append(arv)
soov = "j"
while soov == "j":
# Suvalise s�na v�tmine listist
    arv = random.choice(arvud)
    vastus = input(arv + ' ')
# Listist v�etud s�na on s�nastikule n� indeksiks (v�tmeks).
    if est2ger[arv] == vastus.lower():
        print("�ige!")
    else:
        print("Vale! �ige vastus oleks", est2ger[arv])
    soov = input("Kas j�tkame? [j/e]")

# N�ite m�ttes kirjutatakse arvud faili. �lesande p�stituse seisukohalt ei ole see vajalik.
fm = open("saksa_numbrid.txt","w")
for v2ti in est2ger:
    fm.write(v2ti+' '+est2ger[v2ti]+"\n")
fm.close()
