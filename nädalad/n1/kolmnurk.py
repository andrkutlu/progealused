# Programm leiab kolmnurga pindala, kui on teada alus ja kõrgus
# Sisend: a - alus (float)
#         h - kõrgus (float)
# Väljund: S - pindala (float)

print("Programm aitab Sul leida kolnurga pindala.")
a = float(input("Sisesta kolmnurga alus "))  # Sisend teisendatakse täisarvuks või ujukomaarvuks
h = float(input("Sisesta kolmnurga kõrgus "))
S = a * h / 2                               # Arvutatakse tulemus
print("Kolmnurga pindala on ", S)           # Trükitakse ekraanile vastus
