# Lahendame ülesandeid listidega ja mõtleme sealjuures indeksite peale.
import random as rnd

def ex1():
    """Ülesanne 1 Loendamine uuesti
    Kui loendada on vaja mitme erineva väärtuse esinemist, siis mida teha? Abiks on list, kuhu kogutakse kõik loendurid.
    Listi indeksite ja loendatavate väärtuste vahel luuakse mingi seos. Lihtsamal juhul on indeks loendatav väärtus.
    Nii saab loendada edukalt täisarve.
    Keerulisemad olukorrad on Pythonis lahendatavad sõnastikuga (dictionary). Saab ka teha 2 listi - ühes loendatavad väärtused,
    teises vastavad loendurid.

    Loendurite nullimiseks tuleb kõigepealt teha nullidest koosev list, mille elementide arv vastab loendatavale vahemikule.
    Selleks sobib Pythonis korrutamistehe: loendur = N * [0]

    Ülesanne: genereeri juhuslike arvude generaatoriga palju arve (nt 100000 arvu) vahemikus 0 .. 100.
    Loenda arvud ja väljasta tabel arvude sagedustega. Et rohkem listidega sõbraks saada, võib arvud eelnevalt listi
    kirjutada ja seejärel uue
    tsükli abil loendama hakata."""

    numbers = list()

    for i in range(0, 100):
        rndNr = rnd.randint(0, 99)
        numbers.append(rndNr)

    for i in range(0, 99):

        print(str(i) + " - " + str(numbers.count(i)))


def ex2():
    """Ülesanne 2 Sorteerimine mullimeetodil
    Mullimeetod (bubble sort) töötab järgmiselt: alustades 1. elemendist võrreldakse kahte kõrvuti asuvat elementi ja vahetatakse nad omavahel, kui väiksem on tagapool. Nii toimitakse järjest kõigi kõrvuti asuvate elementidega. Peale esimest korda listi lõppu jõudmist on kõige suurem arv listis viimane. Teised on aga suure tõenäosusega segamini.
    Seega tuleb alustada uuesti algusest ja korrata sama tegevust. Mitu korda on sorteerimiseks vaja list läbi käia? Üle N korra (elementide arv) kindlasti
    mitte. Tegelikult võib piisata ka vähemast. Andmed on sorteeritud, kui kogu listi läbimise ja paaride võrdlemise jooksul
    ei ole vaja teha ühtegi vahetust. Kasuta seda korduste arvu määramiseks.
    Sorteeritav list genereeri juhuslike arvude generaatoriga.
    Tee programmist teine versioon, mis sorteeriks arvud mittekasvavasse järjekorda."""

    numbers = list()
    nr_count = 100

    for i in range(0, nr_count):
        rndNr = rnd.randint(0, 99)
        numbers.append(rndNr)

    print(numbers)
    while numbers != sorted(numbers):
        for i in range(0, 100):
            if i < nr_count-1:
                # print(str(i) + " "+str(i+1))
                if numbers[i] > numbers[i+1]:
                    numbers[i] = numbers[i] + numbers[i+1]

                    numbers[i+1] = numbers[i] - numbers[i+1]

                    numbers[i] = numbers[i] - numbers[i+1]

    print(numbers)


def ex4():
    """Sorteerime veel - seekord proovi läbi valikmeetod (selection sort).
    Meetod töötab järgmiselt:
    Alates esimesest elemendist leia üle kogu massiivi väikseim element,
    vaheta ta massiivis esimesele kohale (NB! esimesel kohal olnud element läheb vähima asemele!)
    Korda tegevust alates 2., 3. jne elemendist, kuni jõuad välja massiivi lõppu.
    """

    numbers = list()
    nr_count = 100

    for i in range(0, nr_count):
        rndNr = rnd.randint(0, 99)
        numbers.append(rndNr)

    

    print(numbers)


ex4()
