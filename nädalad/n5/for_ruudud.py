# Kasutajalt küsitakse mitut arvu ta ruutu tõsta soovib ja seejärel palutakse sisestada
# soovitud arvud. Iga arvu jaoks väljastab programm arvu ruudu.
# Sisend: mitu, arv  : integer
# Väljund: ruut: integer

print("Programm demonstreerib for-tsükli kasutamist.")
print()                     # Lisab ekraanile tühja rea
mitu = int(input("Mitut arvu soovid ruutu tõsta? "))
print("Aitäh!\nHakka nüüd ükshaaval arve sisestama, kohe näed ka tulemust.")    # 
for i in range(mitu):
    arv = float(input(str(i+1) + ". arv "))
# Küsimusse lisatakse arvu jrk-number. Pane käima, siis saad aru.
    ruut = arv**2
    print("Ruut on ",ruut)
print("Tehtud. Kohtumiseni!!")
