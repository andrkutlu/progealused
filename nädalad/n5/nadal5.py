import random


def ex1():
    """Ülesanne 1
    Ülesandeks on leida summa arvudele mingist etteantud vahemikust. Kasutaja sisestab vahemiku
    alguse ja lõpu. Programm annab vastuseks soovitud vahemikku jäävate arvude summa.
    """

    nr1 = 10
    nr2 = 20
    summ = 0

    for nr in range(nr1, nr2):
        summ = summ + nr

    print(summ)


def ex3():
    """
    Koosta väike drilliprogramm korrutustabeli omandamiseks, mida
    näiteks kodus väiksema õe/venna või ema/isa peal katsetada saaksid.
    Programm peab käituma järgmiselt (programmi põhiosa):
    1. Esitab korrutustehte
    2. Ootab kasutajalt vastuse sisestamist
    3. Kontrollib vastuse õigsust
    4. Väljastab, kas vastus oli õige või väär.
    5. Kokku antakse lahendamiseks 10 ülesannet."""

    for ex in range(0, 10):
        nr1 = random.randint(1, 9)
        nr2 = random.randint(1, 9)
        answer = int(input(str(nr1) + " x " + str(nr2) + " = "))
        rightanswer = nr1*nr2
        if answer == rightanswer:
            print("ÕIGE")
        else:
            print("VALE")
        # print(ex)


def ex4():  # tegelt 4
    """a) Sisestatakse tekstilõik ja otsitav sõna. Programm leiab, kas tekstilõigus on olemas otsitav sõna
    ja annab vastuseks, "Sõna ei leidnud" või "Sõna algas ... positsioonist". Täpsemalt küll otsitakse suvalist märkide jada, st
    sõnade tuvastamisega ei pea tegelema.
    b) Kui sõna leiti, küsib programm ja asendussõna. Programm asendab otsitava sõna asendussõnaga ja trükib teksti uuesti välja.

    Abiks on meetodid str.find(), str.replace().
    str asemele tuleb kirjutada selle stringitüüpi muutuja nimi, milles otsid ja asendad.
    """

    sentence = "Kell näitab juba üle keskpäeva, kui Rasmus Mägi tuleb Doha MM-i sportlaste hotelli ootamatult " \
               "kitsukesse fuajeesse, istub kõrge seljatoega pehmele diivanile ja tunnistab, " \
               "et pole eelisest õhtust saadik korralikult magada saanud. Vaid pisut rohkem kui 12 " \
               "tundi tagasi sai lõplikult selgeks, " \
               "et vahepeal juba käes olnud 400 m tõkkejooksu finaalikoht on peost libisenud."

    word = "Rasmus Mägiss"

    if word.lower() in sentence.lower():
        pos = sentence.find(word)
        print("Sõna algas ´"+str(pos) + "´ positsioonist")
        replaceword = input("Asendan sõna " + word + " sõnaga: ")
        sentence = sentence.replace(word, replaceword)
        print(sentence)
    else:
        print("Sõna ei ole lauses")


def ex5(word, shift):
    """Salakirjad on olnud populaarsed aastatuhandeid. Üks lihtsamaid nendest on nn Caesari salakiri. Selle põhimõte on lihtne:
    lepitakse kokku samm, mille kauguselt tähestikust võetakse sõnas oleva tähe asemele asendustäht. Sama sammu /nihet kasutatakse
    iga tähe jaoks. Näiteks sõna "kala" krüpteeruks sammu 4 korral "oepe". Kui samm läheb üle tähestiku lõpu, jätkatakse
    liikumist algusest. Teise kirjelduse (Wikipedia) järgi tehakse nihe vasakule - siis saaks sõnast "kala" "gwhw
    Programmi ülesanne on küsida sõna (või ka lause) ja nihe/samm ning krüpeerida sõna antud sammuga. Olukorra lihtsustamiseks
    eeldame, et tekstis on vaid ladina tähed. Samas peab aga jälgima, et kirjavahemärgid tühikud jms paika jääksid.

    Üheks lahenduseks on kasutada funktsioone:
    ord() - annab ASCII-tabeli alusel tähe koodi ja
    chr() - tuletab koodist uuesti sümboli (ikka ASCII tabeli alusel)
    Leia tähe kood, liida koodile samm ja muuda kood taas täheks."""
    new_word = ""

    for char in word:
        if char.isalpha():
            code = ord(char)
            shift_code = code + shift
            new_word = new_word + str(chr(shift_code))
        else:
            new_word = new_word + str(char)
    print(new_word)
    return new_word


ex5(ex5("TEST LAUSE", 1), -1)
