# Näide funktsioonist, mis arvutab sisendiks oleva listi elementide keskmise

# Funktsiooni keskmine algus


def keskmine(arvud):
    """Funktsioon leiab ja tagastab sisendiks oleva listi väärtuste keskmise"""
    summa = 0
    mitu = len(arvud)
    for i in range(mitu):
        summa = summa + arvud[i]
    tulemus = summa/mitu
    return tulemus            # Funktsioonist väljuva väärtuse määramine
# Funktsiooni keskmine lõpp

# Peaprogrammi algus


jada = []
uus_arv = input("Sisesta arv ")
while uus_arv != "":           # Tsükkel töötab kuni sisendiks midagi on
    jada.append(float(uus_arv))    # Arv lisatakse listi peale teisendust
    uus_arv = input("Sisesta järgmine arv, lõpetamiseks Enter ")
jada_keskmine = keskmine(jada) # Siin kohal jätkub tegevus funktsiooni sees
print("Sisestatud arvude keskmine on", jada_keskmine)
print("Tänan tähelepanu eest!")
# Täitsa lõpp
