import math


def area(r):
    """Koosta funktsioon, mis arvutab raadiuse järgi ringi pindala. Funktsiooni argumendiks
    on raadius ja funktsioon tagastab ringi pindala."""
    return math.pow(r, 2) * math.pi


def distance(x1, y1, x2, y2):
    """Koosta teine funktsioon, mille ülesandeks on kauguse leidmine kahe punkti vahel - argumentideks
    on kahe punkti x ja y koordinaadid ning funktsioon tagastab punktide-vahelise kauguse."""
    x3 = abs(x1-x2)
    y3 = abs(y1-y2)

    z = round(math.sqrt(math.pow(x3, 2) + math.pow(y3, 2)), 2)
    return z


def circle(x1, y1, x2, y2):
    r = round(distance(x1, y1, x2, y2), 2)
    print(area(r))


def ex1():
    """Ülesanne 1 Ringi pindala

    Lisa programm, mis küsib ringi keskpunkti ja ühe ringjoonel oleva punkti koordinaadid ning arvutab välja
    ringi pindala.
    Vastuse leidmiseks kutsutakse välja funktsioonid kauguse leidmiseks ja pindala leidmiseks."""
    circle(00, 2, 2, -1)


def ex3():
    file = open("eesti1.txt", "r")
    content = file.read()
    content = content.upper()

    for c in content:
        print(c)

    chars = sorted(list(set(content)))

    allowed = ['A', 'B', 'D', 'E', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'Ä', 'Õ', 'Ö', 'Ü']
    # print(chars)

    charCount = {}
    totalCount = 0
    for c in allowed:
        charCount[c] = content.count(c)
        totalCount = totalCount + content.count(c)

    print(charCount)
    charCount = reversed(sorted(charCount.items(), key=lambda x: x[1]))

    print(totalCount)

    for c in charCount:
        print(str(c[0]) + " - " + str(c[1]) + " - " + str(round(float(int(c[1])/totalCount)*100,2)) + "%")

ex3()

