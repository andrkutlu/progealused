# Veelkord spordipäeva ülesanne. Seekord kasutades funktsiooni
# Funktsioon miinimumi leidmiseks listist, ztagastatakse indeks,
# et saaks kasutada vastavaid tulemusi ka teistest listidest.


def v6itja(jada):
    "Funktsioon vähima väärtuse leidmiseks listis"
    "Argumendid:"
    "   jada - list andmetega"
    "   tagastatakse vähima elemendi indeks"

    mini = 0
    for i in range(len(jada)):
        if jada[i] < jada[mini]:
            mini = i
    return mini


# Funktsiooni sisendiks on list andmetega ja õpilase nimi, kelle kohta on vaja teada saada.
# Sammud (mõlemad peaksid varasemast tuttavad olema):
# 1. Leida jadast nimele vastav tulemus.
# 2. Tuvastada, mitmes koht selle tulemusega saavutati.
def osaleja_koht(nimed, tulemuseds, nimi):
    "Funktsioon leiab sisestatud nime (nimi) järgi jadast (jada) osaleja poolt"
    "saavutatud koha."

    print("Vabandused, koha leidmise funktsioon tuleb veel lisada, tagastame -1!")
    return -1


# Peaprogrammi algus

nimed = []
tulemused = []
print("Avame faili spordikas.txt, et lugeda sealt andmed.")
fm = open('spordikas.txt')
for rida in fm:  # Tsükkel faili lugemiseks
    rida = rida.strip()
    nimi, tulemus = rida.split()  # Rida tükeldatakse kaheks
    nimed.append(nimi)
    tulemused.append(float(tulemus))
print("NB! Listid näevad välja sellised:")
print(nimed)
print(tulemused)
fm.close()

# Välja kutsutakse funktsioon vähima väärtuse indeksii leidmiseks.
esimene = v6itja(tulemused)
print("Võitja on", nimed[esimene], "tulemusega", tulemused[esimene])
# Kutsutakse välja koha leidmise funktsioon, andes talle ette nime "Maali".
# Loomulikult võib Maali asemel olla suvaline nimi või muutuja.
koht = osaleja_koht(nimed, tulemused, "Maali")
# print(koht)

print("Täname kasutamast! Ilusat päeva jätku :)")
