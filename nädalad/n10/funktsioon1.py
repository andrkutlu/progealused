# Proovime kasutada funktsiooni
# Kasutajalt küsitakse tema ees- ja perekonnanime
# Programm kiidab mõlemat nime

# Funktsiooni algus
def kompliment(nimi):
    # Järgnev string on docstring (dokumentatsiooni genereerimiseks)
    "Funktsioon teeb komplimendi ilusa nime kohta"
    "Sisendparameetriks on string (eeldatavasti mingi nimi)."
    "Funktsioon ei tagasta ühtegi väärtust."
    print("Tere ", nimi)
    print("Sul on väga armas nimi!")
# Funktsiooni lõpp

# Peaprogrammi algus
print("Aga siit algab tegelikult programmi täitmine!")
eesnimi = input("Mis Su eesnimi on? ")
# Funktsioon kompliment kutsutakse välja kaks korda erinevate parameetritega
kompliment(eesnimi)
perenimi = input("Ja perekonnanimi? ")
kompliment(perenimi)
print("Oli tore Sinuga kohtuda!")
