# Muutujatele antakse väärtus ja trükitakse see välja
# On määratud komakohtade arv ja trükivälja laius.
# Kriipsud on teises reas selleks, et saaksid paremini tühikuid lugeda
# ja püüda sel viisil trükivälja laiuse määramise põhimõttest aru saada.
# Mõlemas formaadis on välja laiuseks 10 kohta.
arv_i = 5
arv_f = 6.0
# Nn old-style formatting
print("%10.2f %10d" % (arv_f, arv_i**2))
print("----------------------")

# Nn new-style formatting
print("{:10.2f} {:10d}".format(arv_f, arv_i**2))
print("----------------------")

