# Programmi ülesandeks on tuvastada, kas sisestatud arvud moodustavad maagilise ruudu.
# Sisendiks on kasutaja sisestatud arvud ridade kaupa.
# Programm leiab kõigi ridade ja veergude summad ning kontrollib, kas need summad on võrdsed.
# Kui jah, siis ongi maagiline ruut

print("Järgnev programm kontrollib, kas sisestatud kahmõõtmelise massiivi puhul on meil tegemist maagilise ruuduga.")

# Maagiline _ruut_ küll ütleb, et ridade ja veergude arv peab olema sama.
# Aga järgnev võiks olla ka natuke üldisem näide. Mitte ainult konkreetse ülesande algus.
ridu = int(input("Mitu rida? "))
veerge = int(input("Mitu veergu? "))


arvud = list()
for r in range(ridu):
    rida = list()
    print("Sisestame",r+1,"rida...")
    for v in range(veerge):
        arv = int(input("Sisesta arv"))
        rida.append(arv)
    arvud.append(rida)
    print(arvud)

# Reasummade leidmine kasutades listi indekseid.
summad = list()
for r in range(ridu):
    summa = 0
    for v in range(veerge):
        summa = summa + arvud[r][v]
    summad.append(summa)
print(summad)

# Veerusummade leidmine - see tuleb ise lisada

# Pea- ja kõrvaldiagonaali summade leidmine - ka see tuleb ise lisada

# Maagilisuse kontroll: kui kõik summad võrduvad esimese summaga, on nad ka omavahel järelikult võrdsed.
on_maagiline = True
for r in range(len(summad)):
    if summad[0] != summad[r]:
        on_maagiline = False

#Vastuse väljastamine
if on_maagiline:        # kontrolli kujul if on_maagiline == True: ei peeta heaks stiiliks.
    print("On maagiline ruut.")
else:
    print("Ei ole maagiline ruut.")
