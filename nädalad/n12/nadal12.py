import random as rnd

"""Ülesanne 1 Tärnid

Moodusta ekraanile tärnidest ruut. Tärnide arvu ruudu külgede jaoks sisestab kasutaja. """


def ex1():
    x = 5
    for i in range(1, x):
        print("*"*x)


def ex2():

    x = 100
    x = x+1
    # tab = "{:10.2f} {:10d}"
    format = "{:10.0f}"

    header = format.format(0)
    for i in range(1, x):
        # print(tab + str(i), end="")
        header = header + format.format(i)
        # print(format.format(i), end="")
    print(header)
    for i in range(1, len(header)):
        print("_", end="")
    print("")
    for a in range(1, x):
        print(format.format(a), end="")
        # print(str(a)+tab+"|", end="")
        for b in range(1, x):
            print(format.format(a*b), end="")
        print("")


def gen_matrix(dimenson,max_nr):
    d = []

    for a in range(0, dimenson):
        row = []
        for b in range(0, dimenson):
            # row[b] = rnd.randint(1, 10)
            row.append(rnd.randint(1, max_nr))
        d.append(row)
    return d


def print_matrix(matrix):
    format = "{:4.0f}"
    for x in matrix:
        for y in x:
            print(format.format(y), end="")
        print("")


def diagonal_sum(matrix):
    sum = 0
    for x in range(0, len(matrix)):
        sum = sum + matrix[x][x]
    return sum


def upper_sum(matrix):
    sum = 0
    for y in range(0, len(matrix)):

        for x in range(y, len(matrix)):
            if x < len(matrix)-1:
                sum = sum + matrix[y][x+1]
    return sum


def ex3():
    """On NxN massiiv. Kontrollida, kas on tegemist maagilise ruuduga (st kõigi ridade, veergude ja
    diagonaalide summad on võrdsed). Me ei kontrolli, et arvud erineva oleksid.
    """


a = gen_matrix(5, 2)
print_matrix(a)
print(diagonal_sum(a))
print("Ülemine summa:" + str(upper_sum(a)))



