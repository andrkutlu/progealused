# Moodul "teisenda"
# Moodul sisaldab funktsioone erinevate ühikute vaheliste teisenduste jaoks
# Pane tähele, et üks funktsioon ei püüa teha erinevaid teisendusi, vaid väga konkreetselt ühte.
# See on mõistlik põhimõte, kui teisendusfunktsioone on vaja erinevates tingimustes kasutada.
# Peaprogrammi ülesanne on otsustada, mida teisendada ... ja miks.
# Mooduli kasutamist demonstreerib näide moodul1.py


def inch2cm(yhikud):
    "Funktsioon teisendab sisendiks olevad tollid sentimeetriteks"
    tulemus = yhikud * 2.54
    return tulemus


def cm2inch(yhikud):
    "Funktsioon teisendab sisendiks olevad sentimeetrid tollideks"
    tulemus = yhikud * 1/2.54
    return tulemus


def far2cel(f):
    return (f - 32) * (5 / 9)


def cel2far(c):
    return c * 9/5 + 32


def inch2cm(i):
    return i*2.54


def cm2inch(cm):
    return cm*(1/2.54)
