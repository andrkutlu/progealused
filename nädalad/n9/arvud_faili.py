# Programm genereerib juhuslikud arvud (1000 tk) ning kirjutab need tekstifaili
# Igas reas on üks arv

import random
fm = open('arvud.txt', 'w')
for k in range(1000):
# Arv genereeritakse vahemikus 1..100, teisendatakse faili kirjutamiseks
# stringiks ja lisatakse reavahetus.
    fm.write(str(random.randint(1,100))+"\n")
fm.close()
