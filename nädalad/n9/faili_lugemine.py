# Mõned proovid, et infot failist lugeda.
# Enamus print()-lauseid tegeleb väljatrüki kaunistamisega

print("Järgmise programmiga saad näha, mis on kirjutatud Sinu poolt valitud faili.")
print("Aga pane tähele! See fail peab enne olemas olema.")
f_nimi = input("Sisesta Sind huvitava faili nimi ")
print()
print('*****************************************')
print()
fm = open(f_nimi,"r")
for rida in fm:
# strip lõikab rea lõpust maha reavahetuse sümboli (üldisemalt igasuguse "whitespace'i")
    rida = rida.strip()
    print(rida)
fm.close()
print()
print('*****************************************')
print()
print("Selline see fail oli. Ilusat päeva!")
