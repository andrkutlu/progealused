import math

file_name = "spordikas.txt"


def write_record(name, age):

    failike = open(file_name, 'w')
    failike.write(name+" "+str(age)+"\n")
    failike.close()

def read_records():

    fm = open(file_name, "r")
    for rida in fm:
        rida = rida.strip()
        print(rida)

def ex1():
    """Mari ja Jüri mängivad arvutimängu ja vaidlevad, kumma keskmine skoor on kõrgem.
    Kirjuta programm, mis antud tulemuste põhjal otsustab, kumma mängija keskmine saavutatud punktisumma on kõrgem.
    Sisend. Tekstifaili kesksis.txt esimesel real on mängukordade arv N (1 6 N 6 500 000).
    Järgmisel N real on igaühel suurtäht M või J ja sellest tühikuga eraldatud täisarv P (0 6 P 6
    10 000), kus M tähistab Marit ja J Jürit ning P vastava mängija poolt ühel mängukorral saadud
    punktide arvu.
    Väljund. Tekstifaili keskval.txt ainsale reale väljastada suurtäht M, kui Mari keskmine punktisumma on kõrgem; suurtäht J, kui Jüri oma on kõrgem; või suurtäht V, kui Mari ja Jüri keskmised
    punktide arvud on võrdsed. Kui mängija ei ole mänginud ühtegi mängu, siis lugeda tema keskmiseks tulemuseks 0."""

    points = dict()
    playCount = dict()
    pointsAvg = dict()

    records = open("./ex1/kesksis.txt", "r")
    # print(records)
    recordCount = int(records.readline()) + 1
    # print(recordCount)
    for i in range(1, recordCount):
        line = records.readline().strip()
        record = line.split(" ")
        name = record[0]
        score = record[1]

        if playCount.__contains__(name):
            playCount[name] = playCount[name] + 1
        else:
            playCount[name] = 1

        if points.__contains__(name):
            points[name] = int(points[name]) + int(score)
        else:
            points[name] = score

    records.close()

    records = open("./ex1/keskval.txt", "w")

    for name in playCount:
        score = math.floor(float(points[name]) / float(playCount[name]))
        print(name + " keskmine on " + str(score))
        records.write(name + " " +str(score) + "\n")

    records.close()


def ex3():
    """Ülesanne 3 Kolmas pikkus
    On antud N õpilase pikkused. Kirjutada programm, mis leiab pikkuselt kolmanda õpilase pikkuse.
    Sisend. Tekstifaili pikk.sis esimesel real on täisarv N (3 <= N <= 10 000) ja järgmisel real N
    täisarvu A1, A2, . . . , AN (0 < Ai <= 1000), õpilaste pikkused.
    Väljund. Tekstifaili pikk.val ainsale reale väljastada üks arv, pikkuselt kolmanda õpilase
    pikkus.
    Näide. pikk.sis
    5
    175 173 169 173 171
    pikk.val
    173
    Pikima õpilase kasv on 175 cm, teist ja kolmandat kohta jagavad 173 cm pikkused õpilased."""

    records = open("./ex3/pikkus.sis", "r")
    heights = records.readline().split(" ")
    print(heights)

    heights = list(map(int, heights))

    while heights != sorted(heights):
        for i in range(0, len(heights)):
            if i < len(heights) - 1:

                if heights[i] > heights[i + 1]:
                    heights[i] = heights[i] + heights[i + 1]

                    heights[i + 1] = heights[i] - heights[i + 1]

                    heights[i] = heights[i] - heights[i + 1]
    heights.reverse()

    for i in range(0, len(heights)):
        # print(heights[i])
        if i != len(heights)-1:

            if heights[i] == heights[i+1]:
                print(heights[i])

    print(heights)


ex3()
