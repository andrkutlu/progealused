# Andmete failist lugemine ülesandele "Spordipäev".
# Kasutatakse on stringi meetodit split(), mis sisestatud stringi tükkideks teeb,
# vaikimisi on tükelduskohaks tühik. Tükeldamise tulemuseks on list. Antud näites omistatakse
# listi elemendid (2 tk) eraldi muutujatesse.
# Määrata saab ka muud eraldajat, kasutades selleks meetodil parameetrit:
# rida.split(",") - eraldajaks on koma, tükeldatakse muutujat rida.

nimed = []
tulemused = []
print("Avame faili spordikas.txt, et lugeda sealt andmed.")
fm = open('spordikas.txt','r')
for rida in fm:                      # Tsükkel faili lugemiseks
    rida = rida.strip()
    nimi,tulemus = rida.split()      # Rida tükeldatakse kaheks tühiku kohalt
    nimed.append(nimi)
    tulemused.append(float(tulemus))
print("NB! Listid näevad välja sellised:")
print(nimed)
print(tulemused)
fm.close()

