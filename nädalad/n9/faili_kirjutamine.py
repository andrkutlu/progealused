# Mõned proovid, et infot faili kirjutada.
# Programm trükib faili teksti ja stringiks teisendatud 1-e.

failike = open('tekstifail.txt', 'w')
failike.write("Tere talv!\n")
arv = 1
failike.write(str(arv))
failike.write("Tere kevad!")
failike.close()
