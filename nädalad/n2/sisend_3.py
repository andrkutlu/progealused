# K�sitakse 2 arvu, teisendatakse t�isarvudeks, liidetakse ja v�ljastatakse summa
# arv1, arv2 - liidetavad arvud
# summa - arvude summa

arv1 = input("Sisesta 1. arv ")
arv2 = input("Sisesta 2. arv ")
# int() on funktsioon, mis teksti-t��pi v��rtuse t�isarvuks teisendab.
# Vastasel juhul tekib raskusi arvutamisega. Teisendus toimub aritmeetika avaldises.
# Otstarbekam on siiski muutujate v��rtused kohe peale sisestamist teisendada, et seda
# ei peaks korduvalt tegema.

summa = int(arv1) + int(arv2)
print("Arvude summa on", summa)
