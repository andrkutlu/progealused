# Nime k�simine ja v�ljatr�kk. Print lause teisiti �lesehitatud

minu_nimi = input("Mis su nimi on? ")
print("Tere %s. Lahe nimi sul. Sinuga oli tore tutvuda!" % (minu_nimi))

# Selgitus: %s on formaat, mis v�ljatr�kis asendatakse muutuja "minu_nimi" v��rtusega.
# s �tleb, et tegu on stringiga.
