# muutujale antakse komakohaga v��rtus, seej�rel tr�kitakse see v�lja
# t�psusega kolm kohta peale koma

arv = 13.45685343634636

# Nn old-style formatting
print("Arv %f on kolme komakohani �mardatult %0.3f." % (arv,arv))

# Nn new-style formatting
print("Arv {0} on kolme komakohani �mardatult {0:0.3f}.".format(arv))
