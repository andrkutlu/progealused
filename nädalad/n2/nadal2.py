import math


def ex1(start, end):

    """# Ülesanne 1 Bussireis
# Lähteandmeteks on bussi väljumisaeg ja saabumisaeg. Leida bussisõidu kestvus.
# Võid eeldada, et kogu reis mahub ühe ööpäeva sisse. Vastus anna tundides ja minutites."""

    time1 = start.split(":")
    time2 = end.split(":")

    min1 = int(time1[0]) * 60 + int(time1[1])
    min2 = int(time2[0]) * 60 + int(time2[1])

    duration = min2 - min1
    # print(duration)

    hour = duration // 60
    minute = duration - (hour*60)

    print(hour, ":", minute)


def ex2(gender, height, age, weight):
    """Ülesanne 2 Inimene
    Uurime inimest. Tundide tabelis on link valemitele, mille abil saab leida inimese ideaalkaalu, rasvasuse protsendi, tiheduse,
    ruumala ja pindala.
    Milliseid algandmed oma arvutusteks vajad? Algandmed sisestab kasuta"""

    values = {
        "male": {
            "ideal-weight-multiplier": 0.25,
            "ideal-weight-add": 45,
            "fat": 15
        },
        "female": {
            "ideal-weight-multiplier": 0.225,
            "ideal-weight-add": 40.5,
            "fat": 22
        }

    }

    ideal_weight = (3 * height - 450 + age) * values[gender]["ideal-weight-multiplier"] + values[gender]["ideal-weight-add"]
    fat_percent = round(((weight - ideal_weight)/weight) * 100 + values[gender]["fat"], 2)
    density = 8.9 * fat_percent + 11 * (100-fat_percent)
    volume = round(weight/density, 3)
    area = ((1000 * weight) ** ((35.75 - math.log(weight, 10)) / 53.2)) * ((height ** 0.3) / 3118.2)

    print(ideal_weight)
    print(fat_percent)
    print(density)
    print(volume)
    print(area)

    # ideaalkaal

    # perf_weight =

    # rasva %

    # tihedus

    # ruumala

    # pindala


def ex3():
    """Programm küsib, kui palju maksab kaup ja kui palju ostja raha annab.
    Vastuseks trükib programm ekraanile, millises vääringus ja mitu
    rahatähte tagasi anda tuleb.

    Sa maksid 500 eurot, kaup maksis 133 eurot. Saad tagasi 367 eurot:
0 viiesajalist
1 kahesajalist
1 sajalist
1 viiekümnelist
0 kahekümnelist
1 kümnelist
1 viielist
1 kahelist
0 ühelist"""

    # cost = input("Maksumus:")
    # money = input("Raha:")
    cost = 1
    money = 500

    bills = (1, 2, 5, 10, 20, 50, 100, 200, 500)

    exchange = money - cost
    print("ex: ", exchange)

    for bill in reversed(bills):
        if bill <= exchange:
            exchange = exchange - bill
            print(bill)

    print(exchange)


ex3()
# ex1("14:20", "17:30")

# ex2("female", 178, 22, 75)
# ex2("male", 178, 22, 75)
