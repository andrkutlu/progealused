# Nime (stringit��pi v��rtus, mida ei ole vaja teisendada) k�simine ja v�ljatr�kk
# Sellise print-lausega on t�likas t�hikuid p�ris korda saada.

minu_nimi = input("Mis su nimi on? ")
print("Tere", minu_nimi, ". Lahe nimi sul. Sinuga oli tore tutvuda!")
print("Tere", minu_nimi, ". Lahe nimi sul. Sinuga oli tore tutvuda!", sep='')

# V�rdle v�ljatr�kke ja m�tle t�hikute korrektsusele.
# Parameeter sep=... m��rab, milline s�mbol tr�kitakse koma kohale ehk
# v�ljatr�kis erinevate osade vahele. Vaikimisi on selleks s�mboliks t�hik.
