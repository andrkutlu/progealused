# Muutujatele antakse v��rtus ja tr�kitakse see v�lja
# On m��ratud komakohtade arv ja tr�kiv�lja laius.
# Kriipsud on teises reas selleks, et saaksid paremini t�hikuid lugeda
# ja p��da sel viisil tr�kiv�lja laiuse m��ramise p�him�ttest aru saada.
# M�lemas formaadis on v�lja laiuseks 10 kohta.
arv_i = 5
arv_f = 6.0
# Nn old-style formatting
print("%10.2f %10d" % (arv_f, arv_i**2))
print("----------------------")

# Nn new-style formatting
print("{:10.2f} {:10d}".format(arv_f, arv_i**2))
print("----------------------")

