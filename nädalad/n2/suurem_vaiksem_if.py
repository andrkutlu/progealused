# Programm "uurib" kahte arvu ja v�ljastab nad nii, et v�iksem on ees ja suurem j�rel.
# Sisend: arv1, arv2
# V�ljund: sobiv tekst ja arvud �iges j�rjekorras.
# Klassikaline variant, kus �ks if-lause on teise if-lause sees.

print("Programm tuvastab, kumb sisestatud arvudest on suruems ja kuvab nad suuruse j�rjekorras.")
arv1 = input("Esimene arv ")
arv2 = input("Teine arv ")

if arv1 > arv2:
    print("Esimene arv on suurem.")
    print("Suuruse j�rjekorras: ", arv2, arv1)
else:
    if arv1 < arv2:
        print("Teine arv on suurem.")
        print("Suuruse j�rjekorras ", arv1, arv2)
    else:
        print("Arvud on v�rdsed.")

# Kas tulemus on selline nagu sa ootasid? Proovi erinevate arvupaaridega!
