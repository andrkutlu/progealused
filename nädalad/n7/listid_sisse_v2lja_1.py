# Näide listi elementide töötlemisest, kus tsükli igas korduses tehakse midagi
# listi ühe elemendiga.
# Kõigepealt luuakse list sisestatud nimedest (tühi string lõpetab sisestuse)
# seejärel trükitakse nimed välja
# Peetakse arvet nimede arvu üle
# Sisestamiseks while-tsükkel, väljundiks for-tsükkel

nimed = []                 # Teeme tühja listi
mitu = 0                   # Loendur nimede loendamiseks
uus_nimi = input("Sisesta nimi ")
while uus_nimi != "":      # Tsükkel töötab kuni sisendiks midagi on (ei ole sisestatud tühja stringi).
    mitu += 1
    nimed.append(uus_nimi)    # Nimi lisatakse listi
    uus_nimi = input("Sisesta " + str(mitu + 1)+" nimi, lõpetamiseks Enter ")
for nr in range(len(nimed)):     # Trükitakse välja kõik nimed kasutades listi elemendi indeksit
    print(nr+1, nimed[nr])
print("Tänan tähelepanu eest!")
