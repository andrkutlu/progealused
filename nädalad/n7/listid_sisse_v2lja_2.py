# Näide listi elementide läbimisest
# Kõigepealt luuakse list sisestatud nimedest (tühi string lõpetab sisestuse)
# ja seejärel trükitakse kõik nimed välja
# Sisestamiseks while-tsükkel, väljundiks Pythoni-pärane for-tsükkel

nimed = list()                 # Teeme tühja listi
mitu = int(input("Mitu nime "))
for i in range(mitu):      # Tsükkel töötab kuni sisendiks midagi on
    uus_nimi = input("Trüki nimi ")
    nimed.append(uus_nimi)

# Massiivi saab ka tervikuna välja trükkida, aga see ei näe välja ilus
# ja on pigem kasutatav kontrollväljatrükina.
print(nimed)

# Nimed võetakse listist Pythonile omasel nö foreach-tüüpi tsükli kujul
# NB! Me kasutama valdavalt siiski edaspidi indekseid, etv oleksime rohkem valmis
# massiividega hakkamsaamiseks.
for nimi in nimed:
    print(nimi)
print("Sisestasid", len(nimed), "nime.")  # Funktsioon len() tagastab listi pikkuse
print("Tänan tähelepanu eest!")
