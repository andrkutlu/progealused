# Näide kahe listi paralleelsest kasutamisest
# Ühes listis on nimed, teises nimedega kaasnevad pikkused
# Andmed sisestatakse käsitsi.

N = int(input("Mitu inimest? "))
pikkused = []
nimed = []
for i in range(N):
    nimi = input("Nimi ")
    nimed.append(nimi)
    pikkus = int(input("Pikkus "))
    pikkused.append(pikkus)
print(nimed)
print(pikkused)

# Näide kokkusobivate nimede ja pikkuste väljatrükkimisest
# Et ühe inimese nimi ja pikkus on korraga sisestatud, paiknevad nad ka listides
# samas järjekorras, täpsemalt sama indeksi all. Sama indeksit ongi alljärgnevalt
# ära kasutatud.
for i in range(N):
    print("Nimi:",nimed[i],"ja pikkus", pikkused[i])

    
