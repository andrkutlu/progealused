# Näide suurima väärtuse (maksimumi) leidmise kohta. Ehkki on olemas f-n max(), tuleb osata seda ka ise teha.
# Listi A sisestatakse täisarvud.
# Eeldatakse, et sisestatud väärtused on positiivsed (muutuja maks algväärtustamine!)
# Negatiivsete arvude korral tuleks muutuja maks algväärtust muuta.
# Näitena on toodud kolm sõltumatut varianti suurima väärtuse leidmisest.

A = []                      # Tühi list
mitu = int(input("Mitu arvu listi panna? "))
for i in range(mitu):
    arv = int(input("Arv "))
    A.append(arv)           # Arvu listi lisamine

# Pythoni-pärane indeksiteta variant, muutujasse max jääb suurim arv
maks = 0                    # maks algväärtustamine (pos arvud)
for arv in A:
    if arv > maks:          # Kui leiame senistest suurema arvu, siis peame ta meeles
        maks = arv      
print("Suurim väärtus on",maks)

# Massiivi-stiilis indekseid kasutav variant:
maks = 0
for i in range(mitu):
    if A[i] > maks:
        maks = A[i]
print("Suurim väärtus on", maks)

# Kui on lisaks vaja teada ka indeksit.
# Pythonis on selleks võimalus index()-meetodit kasutada, kuid järgnev variant on universaalne.
# Ja alati ei pruugi ka meetod index() vajalikku tulemust anda.
maks_i = 0
for i in range(mitu):
    if A[i] > A[maks_i]:
        maks_i = i
# maks_i sisaldab suurima elemendi indeksit, mida saab ka koos teise listiga kasutada.
print("Suurim väärtus on", A[maks_i], "see oli ",maks_i+1,"arv.")
