# Mõned näited stringi-meetoditest.
# Stringi esitäht tehakse suureks ja loendatakse a esinemine stringis

nimi =  "maali maasikas"
nimi = nimi.capitalize()   # Esitähe suurendamise meetod
loendur = nimi.count("a")     # Alamstringide loendamise meetod
print("Nimi on ", nimi, "ja selles on ", loendur, " a-d.")
