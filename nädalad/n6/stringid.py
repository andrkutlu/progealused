# Väike näide stringide käitlemisest sümbol-haaval
# Programm ei tee mitte midagi suurt ja ilusat :)

tekst = 'abcde'
# Stringits võetakse järjest kõik sümbolid kasutades selleks indeksit
for i in range(len(tekst)):
     print(tekst[i])
# Stringis tehakse kõik tähed suurtähtedeks.
# Stringi muutmisel tuleb teha uus string, kuid tulemuse võib vanasse muutujasse tagasi panna.
tekst = tekst.upper()    
# "Pütoonilisemal" viisil (indeksit kasutamata) täht haaval väljatrükk:
for t2ht in tekst:
    print(t2ht)
