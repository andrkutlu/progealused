from datetime import date, timedelta
import random
import names
import math


def check_control_number(id_code: str):
    """Check if given value is correct for control number in ID code."""
    kordajad_1 = (1, 2, 3, 4, 5, 6, 7, 8, 9, 1)
    kordajad_2 = (3, 4, 5, 6, 7, 8, 9, 1, 2, 3)
    nr = 0
    kontrollnr = 0
    for x in range(0, 10):
        a = int(id_code[x])
        nr = nr + (kordajad_1[x] * a)

    jääk = nr % 11
    if jääk == 10:
        nr = 0
        for x in range(0, 10):
            a = int(id_code[x])
            nr = nr + (kordajad_2[x] * a)

        jääk = nr % 11
        if jääk == 10:
            kontrollnr = 0
        else:
            kontrollnr = jääk
    else:
        kontrollnr = jääk

    if kontrollnr == int(id_code[10]):
        return True
    else:
        return False


def is_numeric(string):
    """Kas on number."""
    return string.isnumeric()


def check_your_id(id_code: str):
    """
    Kontrolli isikukoodi formaati
    :param id_code: str
    :return: boolean
    """
    result = True
    if len(id_code) < 11:
        print("Too short")
        result = False
    elif len(id_code) > 11:
        print("Too long")
        result = False
    elif not is_numeric(id_code):
        print("Contains letters")
        result = False
    elif len(id_code) == 11 and is_numeric(id_code):
        result = True
    return result


def get_gender(id_code: str):
    """Leia sugu."""
    gender_number = int(id_code[0])
    if gender_number % 2 == 0:
        return "naine"
    else:
        return "mees"


def get_full_year(gender_number: int, year: int):
    """Võta täisaaste."""
    if len(str(year)) == 2 or year == 0:
        years = {1: "18", 2: "18", 3: "19", 4: "19", 5: "20", 6: "20"}
        if year == 0:
            full_year = str(years[gender_number]) + "" + str(year) + "0"
        else:
            full_year = str(years[gender_number]) + "" + str(year)
        return full_year


def get_birthday(id_code: str):
    gender_number = id_code[0]
    year = id_code[1:3]
    month = id_code[3:5]
    day = id_code[5:7]

    year = get_full_year(int(gender_number), year)

    return date(int(year), int(month), int(day))


def get_age(birthday):
    today = date.today()
    return today.year - birthday.year - ((today.month, today.day) < (birthday.month, birthday.day))


def ex1():
    """Programmile on sisendiks on kasutaja nimi ja Eesti Vabariigi kodaniku isikukood (IK).
    Leia IK-st järgmised andmed ja trüki nad võimalikult viisakalt ekraanile:
    1. Sünnipäev
    2. Vanus
    3. Sugu
    4. Kontrolli, kas IK kontrollsumma on õige ehk kontrollnumbri leidmine."""
    # print(check_your_id("39904250267"))
    # print(check_your_id("3510a04250267"))
    # print(get_gender("39904250267"))
    id_code = "39904250267"
    print(get_gender(id_code))
    print(check_control_number(id_code))
    print(get_age(get_birthday("39904250267")))
    # get_age(get_birthday("39904250267"))


def ex2():
    """Ülesanne 2 Keskmine, pikim ja lühim
    Var 1
    Loenda ja väljasta keskmisest lühemate üliõpilaste arv ja pikkused. Milline pikkus on kõige suurem ning milline kõige väiksem?
    Var 2
    Kes on keskmisest lühemad? Kuidas koos kasutada nimesid ja pikkuseid? Tuleb teha teine massiiv, mis sisaldab nimesid.
    Loo oma programmist uus versioon, mis kasutab ka nimesid."""

    persons = {}

    person_count = 10

    avg = 0
    # generate names and heights
    for i in range(0, person_count):
        persons[i] = names.get_first_name(), random.randint(160, 190)
        avg = avg + persons[i][1]
    avg = avg / person_count

    print("Average height:" + str(avg))
    print("Below average:")
    for i in range(0, person_count):
        if persons[i][1] < avg:
            print(persons[i][0] + " - "+str(persons[i][1]))








ex2()

