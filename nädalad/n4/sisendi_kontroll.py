# Näide sisendi kontrollimisest erindit ehk try ... except-lauset kasutades.

print("Proovi sisestada erinevaid väärtuseid ja vaata, kuidas arvuti reageerib")
while True:
    try:
        arv = float(input("Sisesta arv "))
        break
    except ValueError:
        print("Ei ole arv")
print("Said hakkama! Sisestatud arv on", arv)
print("Nüüd tee, mis taha temaga")
