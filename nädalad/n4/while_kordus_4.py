# while-tsükli ja summa leidmise näide
# Sisestatakse ülemine piir ja seejärel küsitakse kasutajalt arve seni,
# kuni nende arvude summa ületab antud piiri


piirarv = int(input("Millise summani peame jõudma? "))
summa = 0
while summa < piirarv:
    arv = int(input("Järgmine arv? "))
    summa = summa + arv             # või summa += arv
#    print("Vahesumma on", summa)    # Võib välja trükkida kõik vahesummad, et paremini näha, mis juhtus.
print("Summa sai selline:",summa)
print("See oli üks väga hea programm")
