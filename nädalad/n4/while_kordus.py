# while-tsükli näide
# muutuja loendur näitab, mitmendat korda tsüklit korratakse ja
# ühtlasi peab arvet korduste arvu ning lõpetamise üle

loendur = 0
while loendur < 10:
    loendur = loendur + 1  # Sama tehte teeks ka omistuslause loendur += 1
    print("Tsüklit täidetakse:", loendur, "korda.")
