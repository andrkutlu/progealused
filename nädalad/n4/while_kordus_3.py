# while-tsükli näide
# Kasutajalt küsitakse parooli. Katsete arv on piiratud kolmega (3)
# programmi ei saa edasi kasutada, kuni ei ole sisestatud õige parool
# NB! Ärge sellisel viisil küll kunagi paroole küsima hakake!
# Tsükli sees loendatakse katsete arvu (muutuja "proove").
# Katsete arvuga on seotud ka tsükli kordamise tingimus.
parool = "kalasalat"
proove = 1
max_valesid = 3
vastus = input("Sisesta parool ")
while vastus != parool and proove < max_valesid:
    proove = proove + 1
    print("Parool on vale. Programmi ei saa edasi kasutada.")
    vastus = input("Sisesta parool uuesti ")
if vastus == parool:
    print("Lõpuks õige! Head kasutamist!!")
else:
    print("Sa ei ole õige inimene. Rohkem proovida ei saa!")

