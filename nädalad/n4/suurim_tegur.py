# Suurima arvu leidmine, millega antud arv jagub (suurim tegur).
# Jaguvust kontrollitakse jäägileidmise tehte abil.
# Lisaks suudab programm tuvastada algarvu.
# Sisend: arv - integer
# Väljund: suurim tegur - integer

print("Programm leiab suurima arvu, millega sisestatud arv jagub")
arv = int(input("Sisest arv "))
tegur = arv // 2          # Suurim tegur ei saa olla üle poole arvust
while tegur > 0:
    if arv % tegur == 0:  # Leitakse jääk, mis tekib arvu jagamisel teguriga
        print(tegur, "on arvu", arv, "suurim tegur.")
        if tegur == 1:    # Kui suurim tegur on 1, peaks meil olema algarv ;)
            print("Tundub, et oleme leidnud algarvu. :)")
        break             # Kui esimene tegur leitud, siis tsükkel katkestatakse
    tegur -= 1            # Proovime 1 võrra väiksema arvuga - ehk jagub
print("Kohtumiseni")

