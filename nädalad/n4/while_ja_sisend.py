#Veel mõned näited andmete sisestamise tsüklitest
#Sisestuse lõpetab Enteri vajutus (ehk tühi string)
#Seetõttu ei teisendata arvu otse inputi järel, vaid hiljem eraldi lauses
#Siin oleks tegelikult parem kasutada järelkontrolliga tsüklit, aga seda paraku
#Pythoni keeles ei ole.
#Seega toimub sellise tsükli "imiteerimine".

# Var 1
summa = 0.0
sisestus = input("Sisesta arv, lõpetamiseks Enter ")
#Esimene sisend on enne tsükli algust, et while'il oleks muutuja, mida kontrollida
while sisestus != "":
    arv = float(sisestus)
    summa = summa + arv
    sisestus = input("Sisesta arv, lõpetamiseks Enter ")
#Viimase lausega saadud sisendit kontrollitakse uue korduse alguses
print("Summa on", summa)

# Var 2
# Ilusam on teha ühe arvuga kõik tööd ära ühes tsükli korduses - sisend, teisendus, arvutus
# Aga lõputut tsüklit ning selle katekstamist break-lausega ei peeta alati heaks tooniks
# Kirjeldatud murest päästaks järelkontrolliga tsükkel.
summa = 0.0
#Alustatakse lõputu tsükliga
while True:
    sisestus = input("Sisesta arv, lõpetamiseks Enter ")
# break käsk katkestab lähima tsükli, st kui sisendiks oli tühi string (sisendi lõpp),
# siis tsükkel katkestatakse.
    if sisestus == "":
        break
    arv = float(sisestus)
    summa = summa + arv
print("Summa on", summa)






