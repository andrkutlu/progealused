# Näide for-tsükli kasutamisest lihtsa määratud kordustearvuga tsükli jaoks.
# Kasutajalt küsitakse kordamiste arv ja tsükli abil trükitakse sama lauset välja
# soovitud arv kordi.
# Sisend:
#     kordusi : integer
# Muutuja "arv" on nn tsüklimuutuja.

print("Programm demonstreerib for-tsükli kasutamist.")
kordusi = int(input("Mitu kordust? "))   # Tsükli korduste arv.
for arv in range(kordusi):               # range() on funktsioon, mis genereerib täisarvude jada 0 .. kordusi-1.
    print(arv)                           # Jadas olevad arvud on näha tänu print-lausele.
print("Viimane arv, mis tsüklimuutujasse muutujasse jääb, on ",arv)

