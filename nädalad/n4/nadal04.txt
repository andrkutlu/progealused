4. n�dal

1. Kuidas vahetada kahes muutujas olevad v��rtused omavahel �ra?
2. Rasvaprotsendivahemike �lesandele m�ned t�iendused ja �ldistused, mis puudutavad vahemike kontrollimist, t��pilisi vigu 
ja if-lause v�imalusi.

Ts�kkel p�hjalikumalt
---------------------
N�ited, mis algavad s�naga "while".
Ts�klit kasutatakse tegevuste kordamiseks. Enne j�rgmist kordust otsustatakse, kas
ts�klis olevaid lauseid tuleb veel t�ita v�i mitte.

Ts�kli kavandamisel tuleb alati l�bi m�telda, milline on:
- ts�kli keha - milliseid lauseid on vaja korrata (ja milliseid mitte!)
- kordamise tingimus - millal ts�kli sisu korratakse ja kuidas tingimus kirja panna?
- korduste arv - mitu korda korratakse - millest korduste arv s�ltub?

NB! On t�iesti OK, kui �he ts�kli sees on teine ts�kkel.


Algoritmid: loendamine ja summeerimine
--------------------------------------
Muutuja v��rtuse suurendamine (increment)
K = K + 1
K senisele v��rtusele liidetakse 1 ja tulemus "pannakse tagasi" samasse muutujasse. 
Muutuja v��rtust suurendatakse �he v�rra. 
Pythonis v�ib ka kirjutada K += 1. Esimene variant on universaalne.

T��palgoritmidena tutvume loendamise ja summeerimisega.

Loendamise idee on j�rgmine: t�isarvuline muutuja (loendur, counter, ...) v�etakse loenduriks. Enne 
loendamist loendur nullitakse (loendur = 0). Iga kord, kui juhtub see, mida loendada vaja (nt leitakse sobiv arv), 
suurendatakse loendurit 1 v�rra (loendur = loendur + 1 v�i loendur += 1). Kui k�ik on l�bivaadatud, �n muutujas loendur
loendamise tulemus.

Summeerimine toimub sarnaselt loendamisega. V�etakse muutuja ja algv��rtustatakse 0-ga (summa = 0). Kogu tegevus
toimub ts�klis. Igas korduses liidetakse summasse uus arv (summa = summa + arv v�i summa += arv) ja saadud uus v��rtus
pannakse tagasi samasse muutujasse. Summa suureneb arvu v�rra.


�lesanne 1 Loendamine
Andmeteks on �li�pilaste kontrollt�� punktid ja t�� maksimaalsed punktid (k�igil �hesugune).
Hindamisel kehtivad protsentides v�ljendatud piirid (A - 91%-100%, B81%-90% jne). Loendada, mitu �pilast 
said t�� eest hindeks B.
Punktid sisestab kasutaja ja peale iga sisestust k�sib programm, kas on veel t�id j��nud.
Lahendust saab laiendada j�rgmiselt:
1) loendatakse k�igi hinnete esinemine, l�puks tehakse kokkuv�te erinevate hinnete esinemisest.
2) teatatakse iga t�� kohta hinne


�lesanne 2 Arvu arvamise m�ng
�ks m�ngija m�tleb arvu etteantud piirides ja teine katsub seda �ra arvata. 
Meie �lesandes - arvuti m�tleb arvu ja inimene hakkab arvama. Arvuti vastab igale arvamisele 
ja t�psustab, kas pakutud arv on liiga suur v�i liiga v�ike.

Kuidas arvuti arvu m�tleb? Selleks on juhuslike arvude generaator. N�iteks sobib funktsioon 
randint(a, b).
Funktsioon tagastab juhusliku t�isarvu x: a <= x <= b
Kasutamise n�ide: arv = randint(1,10)
Sel juhul saab arv t�isarvulise v��rtuse vahemikust [1..10]

Funktsioon randint() paikneb moodulis random, mis tuleb importida.

�ldiselt juhuslike arvude "tootmiseks" tuleb generaator k�igepealt algv��rtustada, milleks on funktsioon seed(). 
See peab tagama, et programmi uuel k�ivitamisel tekiks teistsugune juhuslike arvude jada. Pythonis kaasneb random 
mooduli importimisega automaatselt seed()-i �hekordne k�ivitamine.

V�tame ka teadmiseks, et arvude jada ei saa juhuslikum kui seed()-i iga "arvu tegemise" eel korrata. Pigem vastupidi.
J�relikult ei ole ka import-lauset �ige programmi keskele paigutada. 

Millal m�ng l�petada?
a) arv arvatakse �ra
b) kui liiga palju arvatakse (n�iteks �le 6 korra), siis programm teatab, 
et nii rumal ikka olla ei saa, et niiiii kaua seda �hte �nnetut arvu arvata.

Kui arvuvahemik on 1 .. 100, siis mitme pakkumisega peaks arv kindlasti arvatud saama?

Kui see tehtud, tuleb terve m�ng veel korduma panna - Kas soovid veel m�ngida? 
NB! See t�hendab uue ts�kli lisamist m�nguts�kli �mber. �ra p��a m�nguts�klit k�ike tegema
panna.

Juhuslike suuruste funktsioonimaailm on �pris kirev ja selle kohta saab lugeda Pythoni
dokumentatsiooni lehel: http://docs.python.org/py3k/library/random.html
-----------------------------------

Teema vahetus: Andmed arvuti m�lus.
N�ide arvude murdosa teisendamisest.
Vikipeediast:
http://et.wikipedia.org/wiki/Kahends%C3%BCsteem
n�eb, kuidas toimub murdosade teisendamine kahends�steemi.
Sealt kopi-paste:
K�mmnendarvu murdosa teisendamine kahendarvuks

Murdosa korrutatakse kahega. Kui tekkis t�isosa, see lahutatakse ja saadakse bitt v��rtusega 1, 
kui t�isosa ei tekkinud saadakse bitt v��rtusega 0.

N�iteks murru 0,4 teisendus.
0,4 * 2 = 0,8 t�isosa ei tekkinud, tulemusse bitt 0
0,8 * 2 = 1,6 lahutame t�isosa, tulemusse bitt 1
0,6 * 2 = 1,2 lahutame t�isosa, tulemusse bitt 1
0,2 * 2 = 0,4 t�isosa ei tekkinud, tulemusse bitt 0
0,4 * 2 = 0,8 t�isosa ei tekkinud, tulemusse bitt 0
0,8 * 2 = 1,6 lahutame t�isosa, tulemusse bitt 1
0,6 * 2 = 1,2 lahutame t�isosa, tulemusse bitt 1
0,2 * 2 = 0,4 t�isosa ei tekkinud, tulemusse bitt 0
jne

0,4= 0,01100110..

Ka kahends�steemis saab reaalarve esitada nagu k�mnends�steemis, kusjuures p�rast koma on j�rgukaaludeks 
kahe negatiivsed astmed: 2^-1, 2^-2, 2^-3 jne. K�mnends�steemi arv 0,5 on kahends�steemis seega 0,1.

--------------------------------------------------------------

Proovi teha kodus ka j�rgmisi �lesandeid. Aga et neid on p�ris mitu, siis v�ib l�heneda ka valikuliselt.



�lesanne 3 Algarv
Kas sisestatud arv on algarv? Mis on algarv? Kuidas seda leida? Algarv jagub ainult 1 ja iseendaga.
Sisestata arv. 
Kontrollimiseks proovi, kas ta jagub veel millegagi peale 1 ja iseenda.
K�ige lihtsam on proovida jagada k�igi arvudega vahemikus 2 .. arv // 2. Peaks ka piisama, 
kui �lemiseks piiriks v�tta SQRT(arv).

Mis on ts�kli kordumise tingimuseks? See, et pole leitud arvu, millega uuritav arv jaguks ja on veel arve,
millega proovida jagada.
Kust tulevad arvud, millega jagada? Neid saab ise teha.
Pane k�igepealt jagaja v��rtuseks 2 ja siis suurenda seda igas ts�kli korduses �he v�rra.

Proovi algoritmi �les joonistada!

�lesanne 4 Ruutjuur
Leiame arvust ruutjuure, kasutades selleks Newtoni meetodit (mitte sqrt()-funktsiooni).
Meetodi kirjeldus:
------------------
Alguses tehakse oletus arvu (Arv) ruutjuure kohta (Vana_RJ, see v�ib olla t�iesti suvaline). 
L�htudes esimesest arvust, leitakse uus v��rtus valemist kasutades:
Uus_RJ = (Arv/Vana_RJ + Vana_RJ)/2
V�rreldakse Vana_RJ ja Uus_RJ. Kui nad on (peaaegu) v�rdsed, on ruutjuur leitud.
Kui aga mitte, siis viimati leitud ruutjuur (Uus_RJ) saab Vana_RJ-ks ja arvutust korratakse.

Erinevuse (v�rdumise) kontrollimiseks sobib j�rgmine idee:

EPSILON=10E-09 #M��ratakse t�psus

Ts�kli kontroll: arvuta seni, kui abs(vana_RJ-uus_RJ) > EPSILON #Eelmine ja viimane RJ erinevad rohkem kui t�psus

Kui ts�kkel l�ppeb, on vastus piisavalt t�pne (ehk Uus_RJ-i ja Vana_RJ-i vahe on v�iksem kui m��ratud t�psus).


�lesanne 5 Keskmine
On teada �li�pilaste pikkused ja �li�pilaste arv. Kirjutada programm, mis leiab nende keskmise pikkuse.


�lesanne 6 Spordip�ev
Koolis oli spordip�ev ja lapsed viskasid palli. Iga �pilase kohta on teada tema
parim vise. Eraldi teame Pauli tulemust. Leida, mitmenda koha Paul saavutas.
(Nt: Paul sai palliviskes 4 koha.)
�lesande t�iendus on see, kus tuuakse eraldi v�lja kohtade jagamine.
(Nt: Paul jagas 2 kuni 4 kohta.)


�lesanne 7. Hea pank (ehk "unistada ju v�ib")
Avad pangas kogumishoiuse ja paned hoiusele mingi arvu eurot. Igal palgap�eval (�ks kord
kuu alguses) lisad hoiusele teatud arvu eurosid (igas kuus v�rdselt). Pank lisab 
intressiga teenitud eurod kuu l�pus. Teada on intressim��r aastas. Sellest saame protsendi 
1 kuu kohta, jagades ta 12-ga (intress/12). (Tegelikkuses on see veidi keerulisem.)

Kaks alam�lesannet:
a) Sisestad lisaks summa, mille tahad hoiusele koguda. Programm peab simuleerima 
hoiuse muutumist ja teatama, mitu aastat ja mitu kuud kulub eesm�rgi saavutamiseks.

b) Simuleeri konto seisu muutumist. Tr�ki v�lja konto seis iga hoiustamisaasta 
l�pus 10 aasta jooksul. Tulemus esita tulpadesse
paigutatud tabelina.

Kummagi variandi jaoks koosta eraldi programm. 
