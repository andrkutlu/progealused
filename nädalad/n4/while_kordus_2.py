# while-tsükli näide
# Kasutajalt küsitakse parooli
# programmi ei saa edasi kasutada, kuni ei ole sisestatud õige parool
# NB! Ärge sellisel viisil küll kunagi paroole kasutage, et need otse
# programmi teksti sisse kirjutate!!!
parool = "kalasalat"
vastus = input("Sisesta parool ")
while vastus != parool:
    print("Parool on vale. Programmi ei saa edasi kasutada.")
    vastus = input("Sisesta parool uuesti ")
print("Lõpuks õige! Head kasutamist!!")
