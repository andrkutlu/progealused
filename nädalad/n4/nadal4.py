import random as rnd
import math

def assigning():

    a = 8
    b = 2

    a = a + b
    b = a - b
    a = a - b

    print(a, b)

    kuud = {1: "jaanuar"}

    print(kuud[1])


def get_grade(points):
    # print(points)
    if points in range(91, 100):
        return "A"
    elif points in range(81, 91):
        return "B"
    elif points in range(71, 81):
        return "C"
    elif points in range(61, 71):
        return "D"
    elif points in range(51, 61):
        return "E"
    elif points < 51:
        return "F"


def ex1():
    """Andmeteks on üliõpilaste kontrolltöö punktid ja töö maksimaalsed punktid (kõigil ühesugune).
    Hindamisel kehtivad protsentides väljendatud piirid (A - 91%-100%, B81%-90% jne). Loendada, mitu õpilast
    said töö eest hindeks B.
    Punktid sisestab kasutaja ja peale iga sisestust küsib programm, kas on veel töid jäänud.
    Lahendust saab laiendada järgmiselt:
    1) loendatakse kõigi hinnete esinemine, lõpuks tehakse kokkuvõte erinevate hinnete esinemisest.
    2) teatatakse iga töö kohta hinne"""

    # input max points

    grades = []

    max_points = 100

    while True:
        p = int(input("Next points(0 when none):"))
        # print(p)
        if p == 0:
            print(grades)
            break
        else:
            percent = int((p / max_points) * 100)
            print(percent, " - ", get_grade(percent))
            print()
            grades.append(get_grade(percent))


def ex2():
    """Üks mängija mõtleb arvu etteantud piirides ja teine katsub seda ära arvata.
    Meie ülesandes - arvuti mõtleb arvu ja inimene hakkab arvama. Arvuti vastab igale arvamisele
    ja täpsustab, kas pakutud arv on liiga suur või liiga väike."""
    random_number = rnd.randint(1, 360)
    try_count = 6
    quess_count = 0

    while True:
        if quess_count == 6:
            print("Too many times")
            break
        quess_count = quess_count+1
        nr = int(input("Enter number:"))
        if nr == random_number:
            print("You guessed right")
            break
        elif nr < random_number:
            print("Too small")
        elif nr > random_number:
            print("Too big")


def ex3(number, number_iters=500):
    """Leiame arvust ruutjuure, kasutades selleks Newtoni meetodit (mitte sqrt()-funktsiooni).
       Meetodi kirjeldus:
       ------------------
       Alguses tehakse "oletus" arvu (Arv) ruutjuure kohta (Vana_RJ, see võib olla täiesti suvaline).
       Lähtudes esimesest arvust, leitakse uus väärtus kasutades valemit:
       Uus_RJ = (Arv/Vana_RJ + Vana_RJ)/2
       Võrreldakse Vana_RJ ja Uus_RJ. Kui nad on (peaaegu) võrdsed, on ruutjuur leitud.
       Kui aga mitte, siis viimati leitud ruutjuur (Uus_RJ) saab Vana_RJ-ks ja tegevust korratakse.

       Võrdumise kontrollimiseks määratakse täpsus, mitu komakohta arvudel vähemalt kattuma peaksid. Näiteks 8 kohta peale koma:
       EPSILON=10E-09 #Määratakse täpsus

       Tsükli kordamise tingimus on abs(vana_RJ-uus_RJ) > EPSILON
       See tähendab korratakse seni, kui eelmine ja viimane leitud arv erinevad rohkem kui täpsus.

       Tsükkel lõpetab töö, kui vastus on piisavalt täpne (ehk Uus_RJ-i ja Vana_RJ-i vahe on väiksem kui määratud täpsus)."""

    a = float(number)
    for i in range(number_iters):
        number = 0.5 * (number + a / number)
    # x_(n+1) = 0.5 * (x_n +a / x_n)
    return number


def ex4():
    """On teada üliõpilaste pikkused ja üliõpilaste arv. Kirjutada programm, mis leiab nende keskmise pikkuse."""

    heights = []
    count = 0

    while True:
        height = int(input("Next height(0 when done): "))
        if height == 0:
            print("Average height: " + str(sum(heights) / len(heights)))
            break
        else:
            count = count + 1
            heights.append(height)


def ex5():
    """Koolis oli spordipäev ja lapsed viskasid palli. Iga õpilase kohta on teada tema
    parim vise. Eraldi teame Pauli tulemust. Leida, mitmenda koha Paul saavutas.
    (Nt: Paul sai palliviskes 4 koha.)
    Ülesande täiendus on see, kus tuuakse eraldi välja kohtade jagamine.
    (Nt: Paul jagas 2 kuni 4 kohta.)
    """

    paul = 10

    points = []
    count = 0

    has_paul = False

    paul_pos = 0

    while True:
        point = int(input("Next person points(0 when done): "))
        if point == 0:
            points.append(paul)
            points.sort()
            # points.reverse()

            for i in range(0, len(points)):
                # print(i)
                if points[i] == paul:
                    samecount = points.count(paul)

                    if has_paul is False:
                        paul_pos = i+1
                    has_paul = True
                # else:
                    # print(str(points[i]) + " keegi teine")
            if samecount > 1:
                print("Paul jagas " + str(paul_pos) + " kuni " + str(samecount) + " kohta")
            else:
                print("Paul saavutas " + str(i + 1) + " koha")

            print(points)
            break
        else:
            count = count + 1
            points.append(point)


def ex6():
    """Avad pangas kogumishoiuse ja paned hoiusele mingi arvu eurot. Igal palgapäeval (üks kord
    kuu alguses) lisad hoiusele teatud arvu eurosid (igas kuus võrdselt). Pank lisab
    intressiga teenitud eurod kuu lõpus. Teada on intressimäär aastas. Sellest saame protsendi
    1 kuu kohta, jagades ta 12-ga (intress/12). (Tegelikkuses on see veidi keerulisem.)"""



# for i in range(40, 100):
    # print(i, get_grade(i))
# print(get_grade())


# ex2()
# ex5()

