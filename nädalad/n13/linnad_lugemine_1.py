# -*- coding: ISO-8859-4 -*-
# Programm loeb failist linnade ja vahekauguste info
# Algandmetena on failis linnade nimekiri ja seej�rel iga linna kohta tema
# kaugus teistest linnadest. Faili esimese reas on linnade arv.

import string
linnad = []
kaugused = []
fm = open("linnad.txt","r")
# Faili esimesest reast loetakse linnade arv
mitu = int(fm.readline())
# J�rgnevad linnade nimed - nende arv on muutujas mitu
for indeks in range(mitu):
    linn = fm.readline()
    linn = linn.strip()
    linnad.append(linn)
print("Linnade list on j�rgmine:")
print(linnad)
#Failist loetakse vahekaugused ja splititakse iga rida kauguste listi
for indeks in range(mitu):
### J�rgneva lugemise-teisendamise tegevuse saab ka �ra teha �he (mitte liiga arusaadava) lausega:
###    kaugused.append(list(map(int,fm.readline().split())))
    kaugus = fm.readline()
    kauguste_list = kaugus.split()
# Kaugused teisendatakse arvudeks ja pannakse teise listi
    kaugus_int = []
    for arv_s in kauguste_list:
        kaugus_int.append(int(arv_s))
    kaugused.append(kaugus_int)
fm.close()
print("Ja kauguste list selline:")
print(kaugused)
print("N��d saad hakata k�ike vajalikku leidma!")
