# -*- coding: ISO-8859-4 -*-
# Programm loeb failist andmed ja paneb listidesse, j�rgnev t��tlus tuleb
# vastavalt �lesandele lisada.
# Algandmetena on failis linnade ja kauguste nimekiri niimoodi:
# Linn k1 k2 k3 k4 k5
# k* on kaugus teisest linnast
# linnad - list, milles kirjas k�ik vaadeldavad linnad
# kaugused - list listidest, milles hoitakse kahe linna vahelisi kauguseid.
# Kaugused peavad vastama samale j�rjekorrale, mis linnade listis

linnad = [] 
kaugused = []
# Avatakse andmefail
fm = open("linnad_rivis.txt")
# J�rgnev ts�kkel loeb failist rida haaval ja paigutab andmed listidesse
for linnainfo in fm:
    linnainfo = linnainfo.strip()
    linn = linnainfo.split()        # list, milles elementideks linn ja kaugused
# Indeksi 0 peal on linna nimi - pannakse linnade listi
    linnad.append(linn[0])
# J�rgneb kauguste teisendamine ja abi_listi lisamine
    abi_list = []
# for-ts�kli listi jaoks kasutatakse l�iget - [1:] t�hendab 1-st kuni l�puni
    for kaugus in linn[1:]:
        abi_list.append(int(kaugus))   # Kaugus tehakse enne listi panemist arvuks
    kaugused.append(abi_list)
# Ja n��d vaatame, mis juhtus - ehk tr�kime kontrolliks saadud listid v�lja.
print(kaugused)
print(linnad)
fm.close()

print("\nAndmed on valmis kasutamiseks!")
print("J�udu ja m�istust! :)")
