import math


def print_matrix(matrix):
    format = "{:4.0f}"
    for x in matrix:
        for y in x:
            y = int(y)
            print(format.format(y), end="")
        print("")

def sum_left_to_right(matrix, row):
    sum = 0
    x = len(matrix[row])
    correct_sum = int(matrix[row][x-1])

    for nr in range(0, x-1):
        sum = sum + int(matrix[row][nr])

    if correct_sum == sum:
        return True
    else:
        return False

def sum_up_to_down(matrix, column):
    sum = 0
    y = len(matrix)

    correct_sum = int(matrix[y-1][column])
    for nr in range(0, y - 1):
        sum = sum + int(matrix[nr][column])

    if correct_sum == sum:
        return True
    else:
        return False

def ex1():

    content = open("tabelval/tabelsis.b")

    # for line in range(0,)
    dimensions = True
    dimensionVal = None

    square = []
    for line in content:
        if dimensions:
            dimensions = False
            dimensionVal = line
        else:
            line = line.strip()
            line = line.split(" ")
            square.append(line)

    print_matrix(square)

    print(sum_up_to_down(square, 2))
    # print(sum_left_to_right(square, 1))
    # print(sum_left_to_right(square, 2))


    # print(square)
    # print(dimensionVal)


def print_board(board):
    format = "{:4.0}"
    side_len = int(math.sqrt(len(board)))
    print(side_len)
    side_counter = 0
    # print(len(board))
    for x in board:
        # print(x)
        if side_counter <= side_len-2:
            print(board[x], end="")
            side_counter = side_counter + 1
        else:
            print(board[x])
            side_counter = 0
        # print(board[x], end="")

def ex2():

    board = {}
    board_size = 8

    for a in range(1, board_size+1):
        for b in range(1, board_size+1):
            board[chr(64+a)+str(b)] = "▅"
    # print(table)
    # print(board["A1"])

    board["A3"] = "X"
    print(board)
    print_board(board)
    # print(board)
ex2()