# -*- coding: ISO-8859-4 -*-
# Kahem��tmeilise massiivi ehk listidest koosneva listi nullimine:
# nullidest koosnevad listid tuleb �kshaaval
# "�les ehitada", et v�ltida hilisemaid �llatusi, kus erinevate listide
# asemel on tegelikult �ks list, millele mitu korda viidatakse.

n = int(input('Massiivi k�lg (mitu rida ja veergu on massiivis)? '))
a = []
for r in range(n):
    rida = []
# Siinkohal v�iks listi moodustada ka ts�klit kasutamata,
# nimelt korrutades: rida = n*[0]
    for v in range(n):
        rida.append(0)
    a.append(rida)
print(a)
