import turtle
import math


def ex1():
    """Sisesta arv ja kontrolli, kas on tegemist paarisarvu või paaritu arvuga.
    Kuidas kontrollida, kas arv on paaris või paaritu"""

    arv = 4
    if arv % 2 == 0:
        print("Paaris")
    else:
        print("Paaritu")


def ex2():
    """Sisestatakse 2 arvu. Kontrollitakse, kas suurem arv jagub väiksema arvuga
ja väljastatakse sellekohane vastus. Programm peab ise aru saama, kumb arv suurem on.
Kuidas kontrollida jaguvust?"""

    arv1 = 6
    arv2 = 3
    vastus = 0

    if arv1 > arv2:
        if arv1 % arv2 == 0:
            print("jagub")
            print(arv1 / arv2)
        else:
            print("ei jagu")
    else:
        if arv2 % arv1 == 0:
            print("jagub")
            print(arv2/arv1)
        else:
            print("ei jagu")


def angle(a, b, c):
    return math.degrees(math.acos((c**2 - b**2 - a**2)/(-2.0 * a * b)))


def ex3():
    """Joosepil on kolm kaigast. Kas nendest saab moodustada kolmnurga? Otsustamiseks vajame
    kaigaste pikkuseid, mida tuleb Joosepilt küsida (ehk programmis sisestada)."""

    a = 1
    b = 1
    c = 40
    cons = 5

    if (a + b <= c) or (a + c <= b) or (b + c <= a):

        ai = angle(a, b, c)
        bi = angle(b, c, a)
        ci = angle(c, a, b)

        print(ai, bi, ci)

        wn = turtle.Screen()
        t = turtle.Turtle()
        t.forward(a * cons)
        t.left(ai)
        t.forward(b * cons)
        t.left(bi)
        t.forward(c * cons)
        t.left(ci)
        wn.exitonclick()

        print("Saab moodustada")
    else:
        print("Ei saa moodustada")


def ex4():
    a = 2
    b = 5
    c = 2

    try:
        root = math.sqrt(math.pow(b, 2) - 4 * a * c)

        x1 = (-b + root) / (2 * a)
        x2 = (-b - root) / (2 * a)

        print(x1, x2)

    except ValueError:
        print("reaalarvulised lahendid puuduvad")


def ex6():
    year = 2009

    if year % 4 == 0:
        print("test")


def decToBinary(nr):

    result = ""
    while nr > 0:
        digit = nr % 2
        nr = nr // 2
        print(digit)
        result = str(result) + str(digit)

    print(result[::-1])


decToBinary(19)


