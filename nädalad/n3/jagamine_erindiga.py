# -*- coding: ISO-8859-4 -*-
# Programm jagab kaks sisestatud arvu. Kontrollitakse, kas jagamisel ei teki viga.
# Selleks kasutatakse Pythoni erindit��tluse v�imalusi.
# Sisend: jagatav, jagaja - float
# V�ljund: jagatis v�i veateade

jagatav = float(input("Sisesta jagatav "))
jagaja = float(input("Sisesta jagaja "))
try:
    jagatis = jagatav / jagaja
    print("Vastus on ", jagatis)
except ZeroDivisionError:
    print("Nulliga jagamine ei ole lubatud.")

