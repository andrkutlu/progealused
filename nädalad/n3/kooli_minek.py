# -*- coding: ISO-8859-4 -*-
# Programm jagab soovitusi �ue minemiseks
# Sisend: vastus - string

print("See on v�ga vajalik programm, mis aitab Sul enne kooli minekut olulisi otsuseid teha.")
vastus = input("Kas vihma sajab [ja/ei]? ")
if vastus == "ja":
    print("�ra unusta vihmavarju ja m�istlik oleks s�ita trammiga!")
else:
    print("Liikumine on tervisele kasulik. Jaluta t�na!")
print("Head koolip�eva")


# Seda programmi v�id t�iendada, et jagada soovitusei ka teistsuguste
# ilmastikun�htustega toime tulemiseks :)
