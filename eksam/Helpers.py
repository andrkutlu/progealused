class List:

    def __init__(self, value=None):
        if value is None:
            self.value = []
        else:
            self.value = value

    def my_index(self, element):
        for i in range(0, len(self.value)):
            if self[i] == element:
                return i
        return None

    def my_sum(self):  # summeerimine
        ret = 0
        for nr in self.value:
            ret = ret + nr
        return ret

    def my_avg(self):  # keskmise leidmine
        return self.my_sum() / len(self.value)

    def my_max(self):  # suurima leidmine

        for i in range(0, len(self.value)):
            if i == 0:
                ret = self.value[i]
            else:
                if self.value[i] > ret:
                    ret = self.value[i]
        return ret

    def my_min(self):  # vähima leidmine

        for i in range(0, len(self.value)):
            if i == 0:
                ret = self.value[i]
            else:
                if self.value[i] < ret:
                    ret = self.value[i]
        return ret

    def my_reverse(self):  # tagurpidi list
        return self.value[::-1]

    def my_pop(self, element):  # eemalda element
        i = self.my_index(element)
        self.value = self.value[0:i] + self.value[i + 1:]

    def __getitem__(self, key):
        return self.value[key]

    def __setitem__(self, key, value):
        self.value[key] = value

    def append(self, value):
        self.value.append(value)

    def __str__(self):
        return str(self.value)


# küsida võib sõnastiku pikkust - len(), teised funktsioonid ja meetodid on keelatud.
class Dictionary:
    def __init__(self, value=None):
        if value is None:
            self.value = {}
        else:
            self.value = value

    def clear(self):
        self.value = {}

    def pop(self, key):
        del self.value[key]

    def keys(self):
        return map(self.value)

    def __str__(self):
        return str(self.value)

    def __getitem__(self, key):
        return self.value[key]

    def __setitem__(self, key, value):
        self.value[key] = value


#d = Dictionary({"a": 1, "b": 2})
#d["b"] = 3
#print(d)
#print(d["b"])
#d.pop("a")
#print(d.keys())


def selection_sort(nums):
    # This value of i corresponds to how many values were sorted
    for i in range(len(nums)):
        # We assume that the first item of the unsorted segment is the smallest
        lowest_value_index = i
        # This loop iterates over the unsorted items
        for j in range(i + 1, len(nums)):
            if nums[j] < nums[lowest_value_index]:
                lowest_value_index = j
        # Swap values of the lowest unsorted element with the first unsorted
        # element
        nums[i], nums[lowest_value_index] = nums[lowest_value_index], nums[i]
    return nums


def bubble_sort(nums):
    # We set swapped to True so the loop looks runs at least once
    swapped = True
    while swapped:
        swapped = False
        for i in range(len(nums) - 1):
            if nums[i] > nums[i + 1]:
                # Swap the elements
                nums[i], nums[i + 1] = nums[i + 1], nums[i]
                # Set the flag to True so we'll loop again
                swapped = True
    return nums

def get_grade(points):
    # print(points)
    if points in range(91, 100):
        return "A"
    elif points in range(81, 91):
        return "B"
    elif points in range(71, 81):
        return "C"
    elif points in range(61, 71):
        return "D"
    elif points in range(51, 61):
        return "E"
    elif points < 51:
        return "F"

class Table:

    def __init__(self, columns):
        if columns is None:
            self.header = []
        else:
            self.header = columns

        self.content = {}

        self.rows = []

    def add_row(self, row):
        self.content.update({1: row})

    def print_table(self):
        for row in self.content:
            print(row)


numbers = [10, 5, 10, 4, 8, 9]
print(selection_sort(numbers))

# t = Table(["Nimi", "Sugu", "Pikkus"])
# t.add_row(["Andreas", "Mees", 180])
# t.add_row(["Mingi vend", "Mees", 160])

# print(t.header)
