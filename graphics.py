import turtle
import time
import random
pen = turtle.Turtle()

cons = 5

nrs = [45, 90, 180]

def recurse(n):
    if n > 0:
        pen.left(nrs[random.randint(0, 2)])
        pen.forward(5*random.randint(5, 10))
        recurse(n+1)

recurse(10)

window = turtle.Screen()

pen.forward(150)


turtle.exitonclick()


