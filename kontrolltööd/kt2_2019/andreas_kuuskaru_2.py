"""Esimene rida - kuupäevade arv
järgnevalt:
kuupäev (space) temperatuur

Leida:
- paariskuupäevade temperatuuride keskmine
- loendada mitmel päeval oli temperatuur sellest madalam
"""

file_name = "kt_2.txt"
file = open(file_name, "r")


temperatures = {}
temp_sum = 0
temp_count = 0

f = open(file_name)
lines = f.read().split("\n")
f.close()

days = int(lines[0])

# keskmise arvutus
for day in range(1, days):

    temp_line = lines[day].split(" ")
    if int(temp_line[0]) % 2 == 0:

        temperatures[temp_line[0]] = temp_line[1]
        temp_sum = temp_sum + int(temp_line[1])

temp_avg = round(temp_sum/len(temperatures), 2)

# tabel
print("Paariskuupäevade keskmine: " + str(temp_avg))
print("Päevad mil oli madalam kui keskmine: ")
print("Päev | Temp")
for day in range(1, days):

    temp_line = lines[day].split(" ")
    if float(temp_line[1]) < temp_avg:
        print(temp_line[0] + " | " + temp_line[1])


