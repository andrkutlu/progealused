import random as rnd


def ex1():
    """ Programmi ülesandeks on leida paarisarvude summa. Programm küsib kasutajalt töö alguses,
    mitu täisarvu ta sisestada tahab. Arvuti laseb kasutajal sisestada arve, trükib iga paarisarvu
    korral sobiliku teate ekraanile ning leiab paarisarvude summa.
    Leitud summa trükitakse lõpuks üks kord vastusena välja.
    Paarituid arve võib ka sisestada, kuid nende peale ei regeeri programm mitte mingil viisil."""

    correct_input = False

    while correct_input is False:
        try:
            nr_count = int(input("Täisarvude arv:"))
            correct_input = True
        except ValueError:
            print('Sisesta korrektne arv!')

    input_sum = 0
    input_count = 0
    input_number = None

    while input_count < nr_count:
        try:
            input_number = int(input("Sisesta number:"))
        except ValueError:
            print('Sisesta korrektne arv!')

        if input_number % 2 == 0:
            input_count = input_count + 1
            input_sum = input_sum + input_number
            print(str(input_number) + " on paarisarv")
    print("Paarisarvude summa:" + str(input_sum))


def ex2():
    """Programm küsib kasutajalt kaks arvu: vahemiku alguse ja lõpu.
    Edasi genereerib programm 100 arvu ja loeb üle (loendab),
    mitu arvu nendest mahub eelnevalt sisestatud vahemikku (otspunktid kaasaarvatud).
    Programm trükib välja vahemikku mahtunud arvude arvu. Piiride üle otsusta ise.
    """

    percent = 40  # piirid mõlemal pool protsentides
    print((percent*100)/100)
    correct_input = False
    numbers = []
    while correct_input is False:
        try:
            begin = int(input("Vahemiku algus:"))
            end = int(input("Vahemiku lõpp:"))
            correct_input = True
        except ValueError:
            print('Sisesta korrektne arv!')

    begin_gen = begin + (begin * (percent/100))
    end_gen = end + (end * (percent/100))
    print(begin_gen)
    print(end_gen)
    
    number_count = 0
    
    for i in range(0, 100):
        gen_nr = rnd.randint(begin_gen, end_gen)
        if gen_nr in range(begin, end):
            # print(gen_nr)
            number_count = number_count + 1
        # else:
            # print("!"+str(gen_nr))

    print("Vahemikku " + str(begin) + " - " + str(end) + " mahtunud arvude arv on "+str(number_count))

ex2()

